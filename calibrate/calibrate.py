import cv2
import numpy as np
import matplotlib.pyplot as plt

# Vul deze variablen in aan de hand van de plot (ongeveer van het stuk waar die het verst van het midden af is)
startPlank = 600
stopPlank = 1300

# Crop de afbeelding zodat de lijn overal zichtbaar is (afhankelijk van opstelling)
startCropX = 400
stopCropX = 2700
startCropY = 1000
stopCropY = 2000


# In de onderstaande code wordt de foto geimporteerd, zwart-wit gemaakt en gecropt voor analyse.
afbeelding = "./fotos_2eCalibration/2balk_1triplex.bmp"

img_raw = cv2.imread(afbeelding)
img_raw = img_raw[startCropY:stopCropY, startCropX:stopCropX]             
img_BGR2GRAY = cv2.cvtColor(img_raw, cv2.COLOR_BGR2GRAY)
ret, thresh1 = cv2.threshold(img_BGR2GRAY, 20, 255, cv2.THRESH_BINARY)
cv2.imwrite('threshold.jpg', thresh1)
plt.imshow(img_raw)
plt.show() 


# Het volgende gedeelte van de code is gehaald uit imgProcessing, zorgt voor 2 arrays met x en y waarden
step_size = 1

x = thresh1.shape[1]
y = thresh1.shape[0]

x_coord = np.zeros(x) # Y-Coordinaten van de foto
y_coord = np.zeros(x) # X-Coordinaten van de foto
    
for i in range(0, x, 1): # Scannen de afbeelding per pixel
    crop_img = thresh1[0:y, i:step_size]
    step_size = step_size + 1
    
    white_line = np.argwhere(crop_img == 255) # Filter de witte lijn, de laser
 
    xval = [pixel[0] for pixel in white_line]
    yval = [pixel[1] for pixel in white_line]
 
    x_val_e = -1 * np.median(xval) # Dit is y -> x_coord #inverted?
    y_val_e = np.median(yval) # Dit is x -> y_coord #inverted?
    x_coord[i] = x_val_e  # Dit is y
    y_coord[i] = (y_val_e + i) # Dit is x


# Bepaling base line
y_1 = np.mean(x_coord[0:500])    
y_2 = np.mean(x_coord[x-500:x])

x_1 = y_coord[0]
x_2 = y_coord[x-1]  
 
# Laat de plot zien        
plt.plot(y_coord, x_coord)
plt.show()

# Zet de plot recht zodat het mogelijk wordt om de de lijn te bepalen. 
skew_correction = (y_2 - y_1) / (x_2 + x_1)

for i in range(x):
    correction = y_coord[i] * skew_correction
    x_coord[i] = x_coord[i] - correction

y_1 = np.mean(x_coord[0:1])    
y_2 = np.mean(x_coord[x-1:x])

lineAVG = (y_2 + y_1)/2

# Zet de laser lijn op 0 in de plot
for i in range(x):
    x_coord[i] = x_coord[i] - lineAVG 
 
# Bereken de gemiddelde afstand van de baseline tot de verhoogde lijn
avg = np.average(x_coord[startPlank:stopPlank])   
     
# Laat de plot zien        
plt.plot(y_coord, x_coord)
plt.show()

# Print de gemiddelde dikte van de plank in pixels
print("Dikte plank in pixels: ", abs(avg))
        
    
    

