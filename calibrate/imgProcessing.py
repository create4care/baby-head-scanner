import cv2
import numpy as np
import matplotlib.pyplot as plt
import math

from scipy.signal import savgol_filter


#Deze onderstaande code wordt de foto geimporteerd en zwart-wit gemaakt voor analyse.

# Lees de afbeelding in.
img_raw = cv2.imread("2eKalibratie_blaadje.jpg")

# Knip de afbeelding indien nodig.
#img_raw = img_raw[500:2000, 460:2390]

# Laat de afbeelding zien op het scherm.
plt.imshow(img_raw)
plt.show() 

# Converteer de kleuren afbeelding naar grijswaarden.
img_BGR2GRAY = cv2.cvtColor(img_raw, cv2.COLOR_BGR2GRAY)

# Converteer de grijswaarden afbeelding naar binair(zwart/wit).
ret, thresh1 = cv2.threshold(img_BGR2GRAY, 10, 255, cv2.THRESH_BINARY)

# Laat de afbeelding zien op het scherm.
plt.imshow(thresh1)
plt.show() 

x_imageSize = thresh1.shape[1]
y_imageSize = thresh1.shape[0]

# Declareer de stap groote, door in stappen door de afbeelding te gaan, wordt er een gewenste afronding aan toe gevoegd.
step_size = 5

def getLinePosition(step_size):    
    x_coord = np.zeros(x_imageSize) # Y-Coordinaten van de foto
    y_coord = np.zeros(x_imageSize) # X-Coordinaten van de foto    
      
    # Loop door de afbeelding met de gegeven stapgroote.
    for i in range(0, x_imageSize, 1): 
    
        # Knip een kleinstukje uit de afbeelding, dit is de stap.
        crop_img = thresh1[0:y_imageSize, i:step_size]
        step_size = step_size + 1
        
        # Selecteer de witte pixels in het geknipte stukje van de afbeelding.
        white_line = np.argwhere(crop_img == 255)

        # Splits de white_line array op in afzondelijke arrays voor x en y
        yval = [pixel[0] for pixel in white_line]
        xval = [pixel[1] for pixel in white_line]
        
        x_val_e = 1 * np.median(xval) # Neem de mediaan van de lijn.
        y_val_e = np.mean(yval) 

        x_coord[i] = (x_val_e  + i) # plaats de X waarde in de array.
        y_coord[i] = y_val_e # Dit is Y waarde.
    
    plt.plot(x_coord, y_coord)
    plt.title("acquired line")
    plt.show()
    
    return ( x_coord,y_coord )

x_coord, y_coord = getLinePosition(step_size)

def baseLine(testPixelWidth):
    # Bepaling base line
    y_start = np.mean(y_coord[0:testPixelWidth])    
    y_stop = np.mean(y_coord[x_imageSize-testPixelWidth:x_imageSize])

    x_start = x_coord[0]
    x_stop = x_coord[x_imageSize-1]  
    return (y_start, y_stop, x_start, x_stop)
 
# Bepaal de baseline.
y_start, y_stop, x_start, x_stop = baseLine(10)

# Bepaal de schuinte van de baseline. 
skew_correction = (y_stop - y_start) / (x_stop + x_start)

# Zet de plot recht.
for i in range(x_imageSize):
    y_coord[i] = y_coord[i] - x_coord[i] * skew_correction

# Bepaal opnieuw de baseline, deze keer voor het op nul zetten van de baseline.
y_start, y_stop, x_start, x_stop = baseLine(10)

# Neem het gemiddelde
lineAVG = (y_stop + y_start)/2

# Zet de baseline(laserline) op nul.
for i in range(x_imageSize):
    y_coord[i] = y_coord[i] - lineAVG 
    
plt.plot(x_coord, y_coord)
plt.title("after skew correction and rebasing")
plt.show()

print(max(y_coord))

# Werk de pixel onnauwkeurigheden weg, omdat deze onnauwkeurigheidjes ervoor zorgen dat de lijn langer wordt dan dat deze in de werkelijkheid is.
y_coord = savgol_filter(y_coord, window_length = 351, polyorder = 3)
plt.plot(x_coord, y_coord)
plt.title("smooth" )
plt.show() 

print(max(y_coord))



# Declareer een x en y array voor de metrische waarden. 
real_x = np.zeros(x_imageSize) 
real_y = np.zeros(x_imageSize) 
 
# declareer de constanten 
rpc = 0.000220081 # Radialen per pixel pitch
ro = 0.2281592 # Offset in radialen
h = 114 # Afstand tussen camera en lijn laser
total_height = 490 # Aan camera recht naar de grond.
#real_width_of_image = 200/1950 # Werkelijke breedte van de afbeelding
mm_per_pixel = 200/1930 #(real_width_of_image/x)

# Bereken de y door middel van de driehoek berekening.            
def calculateReal_Y():
    # Bepaling hoogte
    for k in range(0, x_imageSize):    
    
        # bereken de objectafstand
        pfc = y_coord[k]
        theta = rpc * pfc + ro 
        tan_theta = math.tan(theta)
        real_y[k] = (total_height - (h /tan_theta))  
    
        #Elke waarde onder 0 heeft geen hoogte
        if real_y[k] <= 0:
            real_y[k] = 0
        else:
            real_y[k] = real_y[k]
            
calculateReal_Y()            
            
# Bereken de werkelijke x
def calculateReal_X():  
    for k in range(0, x_imageSize):        
        #Bepaling echte X-Coordinate
        real_x[k] = x_coord[k] * mm_per_pixel
  
calculateReal_X()      


print("de waarde van de top: ",max(real_y))
    
plt.plot(real_x, real_y)
plt.title("height in mm")
plt.show()

# Bereken de booglengte door middel van de stelling van pythagoras.
def BoogLengte(start, stop):
    prev_y = 0
    
    # Differenceer de y waarden.
    for i in range(0, x_imageSize):
        y = real_y[i] - prev_y   
        prev_y = real_y[i]
        real_y[i] = y 
    
    # Toon de gedifferenceerde waarde.
    plt.plot(real_x[start:stop], real_y[start:stop])
    plt.title("het y verschil per pixel" )
    plt.show()  
    
    # Pas de stelling van pytagoras
    sum = 0
    for y in real_y[start:stop]:
        sum += math.sqrt((mm_per_pixel)**2 + (y**2))
    print("cumulate_boog")    
    print(sum)

# bereken de booglengte, de start en de stop worden meegegeven. 
BoogLengte(460,2390-step_size) 
